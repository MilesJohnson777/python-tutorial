# for i in range(10):
#     print('i = ', i)

###########################################

# your_float = input('enter a float: ')
# your_float = float(your_float)
#
# print('round to 2 decimal places : {:.2f}'.format(your_float))

###########################################

# import random
#
# rand_num = random.randrange(1,51)
#
# i = 1
# while i != rand_num:
#     i += 1
#
# print('the random number is : ', rand_num)

#############################################

i = 1

while i <= 20:
    if i % 2 == 0:
        i += 1
        continue

    if i == 15:
        break

    print('Odd : ', i)

    i += 1