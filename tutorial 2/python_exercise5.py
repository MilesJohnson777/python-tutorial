#draw a pine tree on the screen
# take input from user to get the number of rows in the tree

rows = int(input('enter how many rows for this tree: '))
space = rows - 1
leaves = 1
stump_space = space

while rows != 0:
    for i in range(space):
        print(' ', end="")
    for i in range(leaves):
        print('#', end="")
    print()
    space -= 1
    leaves += 2
    rows -= 1

for i in range(stump_space):
    print(' ', end="")

print('#')