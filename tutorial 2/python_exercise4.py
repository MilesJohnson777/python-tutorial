#have the user enter their investment amount and expected interest
#each year their investment will increase by their investment * interest rate
#print out earnings

current_year = 2017

invest, rate = input('enter your investment and expected interest rate: ').split()
invest = float(invest)
rate = float(rate) * .01

for i in range(11):
    current_year += 1
    invest = invest + (invest * rate)
    print('Your account in year {} will be {:.2f}.'.format(current_year, invest))