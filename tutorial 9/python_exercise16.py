'''
create 2 warriors ~ Sam and Paul.
each warrior has a health pool, max attack value and max block value.
the warriors will take turns dealing a random amount of damage
until either warrior's health pool is depleted.
~ Game Over.
'''

import random

class Warrior:

    def __init__(self, name='', healthPool=10, maxAttack=9, maxBlock=8):
        self.name = name
        self.healthPool = healthPool
        self.maxAttack = maxAttack
        self.maxBlock = maxBlock

    def attack(self):
        value = random.randrange(1, self.maxAttack)
        print('{} attacks for {} damage'.format(self.name, value))
        return value

    def block(self, attackVal):
        value = random.randrange(0, self.maxBlock)
        if value == 0:
            print('{}\'s block failed!'.format(self.name))
            return attackVal
        if attackVal <= value:
            print('{} has blocked all damage!'.format(self.name))
            return 0
        else:
            attackVal -= value
            print('{} blocked {} points of damage, taking only {} damage'
                  .format(self.name, value, attackVal))
            return attackVal

    def hereComesThePain(self, attackVal):
        self.healthPool -= attackVal
        print('{} now has {} points of health remaining.'.format(self.name, self.healthPool))

    def take_a_break(self):
        self.healthPool = 10
        print('{} is refreshed and ready for the next battle.'.format(self.name))

class Battle:

    def __init__(self, coinFace=0, warrior1=1, warrior2=2):
        self.coinFace = coinFace
        self.warrior1 = warrior1
        self.warrior2 = warrior2

    def coin_flip(self):
        self.coinFace = random.randrange(1,3)
        print('Flipping coin to see who attacks first...')
        return self.coinFace

    def end_of_battle(self, winner):
        print('{} emerges victorious!!'.format(winner.name))
        print('End of battle!')
        print()

    def fight(self, warrior1, warrior2):
        while True:
            warrior2.hereComesThePain(warrior2.block(warrior1.attack()))
            if warrior2.healthPool <= 0:
                self.end_of_battle(warrior1)
                print()
                break
            warrior1.hereComesThePain(warrior1.block(warrior2.attack()))
            if warrior1.healthPool <= 0:
                self.end_of_battle(warrior2)
                print()
                break
            print()

    def action(self, warrior1, warrior2, coin):
        if coin == self.warrior1:
            print('Coin landed heads up!')
            print('{} wins the coin flip and gets to attack first!'.format(warrior1.name))
            print()
            self.fight(warrior1, warrior2)
        elif coin == self.warrior2:
            print('Coin landed tails up!')
            print('{} wins the coin flip and gets to attack first!'.format(warrior2.name))
            print()
            self.fight(warrior2, warrior1)


Sam = Warrior()
Sam.name = 'Sam'
Paul = Warrior()
Paul.name = 'Paul'

round1 = Battle()
round2 = Battle()
round3 = Battle()

def main():
    print('It\'s time for Round 1 to begin!')
    round1.action(Sam, Paul, round1.coin_flip())

    print('*******************************')
    Sam.take_a_break()
    Paul.take_a_break()
    print('*******************************')
    print()

    print('It\'s time for Round 2 to begin!')
    round2.action(Sam, Paul, round2.coin_flip())

    print('*******************************')
    Sam.take_a_break()
    Paul.take_a_break()
    print('*******************************')
    print()

    print('It\'s time for Round 3 to begin!')
    round3.action(Sam, Paul, round3.coin_flip())

main()
