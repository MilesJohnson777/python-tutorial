## object oriented

# class Dog:
#
#     def __init__(self, name='', height=0, weight=0):
#         self.name = name
#         self.height = height
#         self.weight = weight
#
#     def run(self):
#         print('{} the dog runs'.format(self.name))
#
#     def eat(self):
#         print('{} the dog eats'.format(self.name))
#
#     def bark(self):
#         print('{} the dog barks'.format(self.name))
#
# def main():
#     spot = Dog('Spot', 34, 56)
#
#     spot.bark()
#
#     bowser = Dog()
#
#     bowser.eat()
#
#     bowser.name = 'Bowser'
#
#     bowser.run()
#
# main()

######################################################################################

class Square:
    def __init__(self, height=0.0, width=0.0):
        self.height = height
        self.width = width

    @property
    def height(self):
        print('retrieving the height...')
        return self.__height

    @height.setter
    def height(self, value):
        self.__height = value

    @property
    def width(self):
        print('retrieving the width...')
        return self.__width

    @width.setter
    def width(self, value):
        self.__width = value

    def getArea(self):
        return self.__width * self.__height

def main():
    aSquare = Square()

    try:
        height = float(input('Enter the height: '))
    except ValueError:
        print('please only enter numbers for height.')
        main()
    try:
        width = float(input('Enter the width: '))
    except ValueError:
        print('please only enter numbers for width.')
        main()

    aSquare.height = height
    aSquare.width = width

    print('Height: ', aSquare.height)
    print('Width: ', aSquare.width)
    print('The area is: {:.2f}'.format(aSquare.getArea()))
    exit(0)

main()
