## recursive functions and dictionaries

## dictionaries

# laskaDict = {'name':'Laska', 'age':3, 'breed':'Daschund'}
#
# print(laskaDict['name'])
#
# laskaDict['cute'] = True
#
# print('does he have a name? ', 'name' in laskaDict)
#
# print(laskaDict.values())
#
# print(laskaDict.keys())
#
# for k, v in laskaDict.items():
#     print(k,v)
#
# print(laskaDict.get('lName', 'N/A'))
#
# del laskaDict['age']
#
# for i in laskaDict:
#     print(i)
#
# laskaDict.clear()
#
# employees = []
#
# try:
#     fname, lname = input('enter first and last name: ').split()
#     employees.append({'fname' : fname, 'lname' : lname})
#     print(employees)
# except ValueError:
#     print("please enter BOTH first and last names.")
#
#########################################################################33

## recursive functions

def factorial(num):
    if num <= 1:
        return 1
    else:
        result = num * factorial(num - 1)
        return result

print(factorial(4))

