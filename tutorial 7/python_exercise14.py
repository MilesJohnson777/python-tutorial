# create a recursive function that generates a fibonacci sequence

def fibo(num):
    if num == 0:
        return 0
    elif num == 1:
        return 1
    else:
        result = fibo( num -1) + fibo(num - 2)
        return result

print(fibo(6))

