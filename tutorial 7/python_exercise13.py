''' 
create a list of customer dictionaries.
cycle a loop for the menu asking to input a new 
customer or not. loop quits when no is input, then print all names.
'''

customers = []

while True:
    try:
        reply = str(input('would you like to enter a new customer? (y/n): '))
        reply = str.lower(reply)
        if reply == 'y':
            try:
                fname, lname = input('enter first and lastname: ').split()
                customers.append({'first_name': fname, 'last_name': lname})
            except ValueError:
                print('please enter both first and last name separated by a space.')

        elif reply == 'n':
            # TODO print all names ~ make a for loop
            print()
            print("here is the complete list of customer names: ")
            for i in customers:
                print(i['first_name'], ' ', i['last_name'])

            print()
            print('Until next time, bye!')
            break
        else:
            print('please enter either \'y\' for yes, or \'n\' for no and exit.')

    except ValueError:
        print('please enter either \'y\' for yes, or \'n\' for no and exit.')
        continue


