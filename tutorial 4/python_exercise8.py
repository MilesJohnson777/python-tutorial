# create an acronym generator
# ask the user to enter a string
# create an acronym from entered string

user_string = str(input('Enter some words to create an acronym: '))
acro = ''
temp_acro = ''
string_list = user_string.split()

print(string_list)

for i in string_list:
    temp_acro = i
    for j in temp_acro:
        acro += j
        break

acro = acro.upper()
print('.'.join(acro) + '.')
