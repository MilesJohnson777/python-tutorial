# create a ceasar's cypher
# take input from user and shift unicode values to encrypt the message

mess = str(input("Enter a message to be encrypted with ceasar's cypher: "))

ceasar = ''

for i in mess:
    temp = ord(i)
    temp += 7
    ceasar += chr(temp)

print('message encrypted as : ', ceasar)
