# # string functions
#
# string1 = '     this is a sentence that had too much white space      '
#
# # removes whitespace from left
# string1 = string1.lstrip()
# # removes whitespace from right
# string1 = string1.rstrip()
# # removes whitespace from both sides
# #string1 = string1.strip()
# print(string1)
#
# print(string1.capitalize())
#
# print(string1.upper())
#
# print(string1.lower())
#
# a_list = ['bunch', 'of', 'words', 'and', 'stuff']
# print(' '.join(a_list))
#
# string2 = string1.split()
# for i in string2:
#     print(i)
#
# print("how e's in string1: ", string1.count('e'))
#
# print('where is space: ', string1.find('space'))
#
# print(string1.replace('had ', 'has '))

#######################################################################

letter_z = 'z'
num_3 = '3'
pi = '3.14'
a_space = ' '

print('is z either a letter or number: ', letter_z.isalnum())

print('is z a letter: ', letter_z.isalpha())

print('is z a number: ', letter_z.isdigit())

print('is z lowercase: ', letter_z.islower())

print('is z uppercase: ', letter_z.isupper())

print('is space a space: ', a_space.isspace())

print('is 3 a number: ', num_3.isdigit())

def isfloat(str_val):
    try:
        float(str_val)
        return True
    except ValueError:
        return False

print('is pi a float: ', isfloat(pi))
