# create a function the receives a algebraic equation
# in string format and solves for x

def alg_equation(string):
    parsed = string.split()
    operator = parsed[1]
    num1 = int(parsed[2])
    num2 = int(parsed[4])

    if operator == '+':
        print('X = ', num2 - num1)
    elif operator == '-':
        print('X = ', num2 - num1)
    elif operator == '*':
        print('X = ', num2 / num1)
    elif operator == '/':
        print('X = ', num2 * num1)

print()
print('Enter an algebraic equation and be sure to use this format:')
print('x + num = num')
print('dont forget to separate everything with spaces!!')
equation = str(input())

alg_equation(equation)