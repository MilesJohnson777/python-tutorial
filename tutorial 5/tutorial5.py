## functions
#
##
#
# def add_numbers(num1, num2):
#     return num1 + num2
#
# print("5 + 4 = ", add_numbers(5,4))
#
#####################################################
#
## var defined in function available only in that function

# def assign_name():
#     name = "doug"
#
# assign_name()
#
# print(name)
#
#################################################
#
## proper way to return values - return

# def change_name(name):
#     return 'mark'
#
# name = 'tom'
#
# name = change_name(name)
#
# print(name)
#
####################################################

## using global vars

# gbl_name = 'sally'
#
# def change_name():
#     global gbl_name
#     gbl_name = 'sammy'
#
# change_name()
#
# print(gbl_name)
#
######################################################

## returns None
#
# def get_sum(num1 , num2):
#     sum = num1 + num2
#
# print(get_sum(5,4))

#########################################################

## return multiple values

# def mult_divide(num1, num2):
#     try:
#         return (num1 * num2), (num1 / num2)
#     except ZeroDivisionError:
#         print('cannot divide by zero')
#         return (num1 * num2)
#
# try:
#     mult, div = mult_divide(2, 0)
#     print(mult, div)
# except TypeError:
#     mult = mult_divide(2, 0)
#     print(mult)

######################################################

## return a list of primes
#
# def is_prime(num):
#     for i in range(2, num):
#         if (num % i) == 0:
#             return False
#     return True
#
# def get_primes(max):
#     list_of_primes = []
#     for num1 in range(2, max):
#         if is_prime(num1):
#             list_of_primes.append(num1)
#
#     return list_of_primes
#
# print(get_primes(80))

#################################################################

## unknown args

# def sumall(*args):
#     sum = 0
#
#     for i in args:
#         sum += i
#
#     return sum
#
# print("sum: ", sumall(1,2,3,4,5,6,7,8,9))
#
# # out of curiousity
#
# sum_list = [1,2,3,4,5,6,45,4,3,4,5,54,9]
#
# print('sum from list: ', sumall(sum_list)) # NOPE!! doesn't work...

##############################################################################

import math

def get_area(shape):
    shape = shape.lower()

    if shape == "rectangle":
        rectangle_area()
    elif shape == "circle":
        circle_area()
    elif shape == "triangle":
        triangle_area()
    else:
        print("Please enter a rectangle or a circle.")

def triangle_area():
    length = float(input("Enter length of any side of the triangle: "))
    width = float(input("Enter length of another side of the triangle: "))
    area = length * width / 2

    print("The area of the triangle is {:.2f}".format(area))

def rectangle_area():
    length = float(input("Enter the length: "))
    width = float(input("Enter the width: "))
    area = length * width

    print("The area of the rectangle is ", area)

def circle_area():
    radius = float(input("Enter the radius: "))
    area = math.pi * (math.pow(radius, 2))

    print("The area of the circle is {:.2f}".format(area))

def main():
    shape_type = input("Get area for what shape: ")

    get_area(shape_type)

main()