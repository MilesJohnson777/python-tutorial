# while True:
#     try:
#         number = int(input('Please enter a number: '))
#         break
#     except ValueError:
#         print('you did not enter a number...')
#     except:
#         print('an unknown error occurred')
# print('Thank you for entering number {}'.format(number))

################################################################

# Math functions

import math

# print('ceil(4.4) = ', math.ceil(4.4))
# print('fabs(-4.4) = ', math.fabs(-4.4))
# print('floor(4.4) = ', math.floor(4.4))
# print('factorial(4) = ', math.factorial(4))
# print('fmod(4, 4) = ', math.fmod(4, 4))
# print('trunc(4.4) = ', math.trunc(4.4))
# print('pow(4, 4) = ', math.pow(4, 4))
# print('sqrt(4) = ', math.sqrt(4))
# print('math.e = ', math.e)
# print('math.pi = ', math.pi)
# print('math.exp(4) = ', math.exp(4))
# print('math.log(4) = ', math.log(4))
# print('math.log(1000, 10) = ', math.log(1000, 10))
# print('math.log10(1000) = ', math.log10(1000))
# print('math.degrees(1.5708) = ', math.degrees(1.5708))
# print('math.radians(90) = ', math.radians(90))

################################################################

# from decimal import Decimal as D
#
# sum = D(0)
# sum += D('0.1')
# sum += D('0.1')
# sum += D('0.1')
# sum -= D('0.3')
# print('using decimal module, sum = ', sum)
# print('without decimal module:  .1 + .1 + .1 - .3 = ', .1 + .1 + .1 - .3)

##############################################################

# var types

# print(type(987))
# print(type(9.87))
# print(type('hello'))

###################################################################

# string arrays

some_string = 'the quick brown fox jumped over the lazy dog'

print(some_string[0])
print(some_string[-1])
print(some_string[3+5])
print('length: ', len(some_string))
print(some_string[0:4])
print(some_string[9:])

print(some_string * 5)

num_string = str(6536534)
print(num_string)

for char in some_string:
    print(char)

for i in range(0, len(some_string)-1, 2):
    print(some_string[i] + some_string[i+1])

#unicode: A - Z: 65 - 90
#         a - z: 97 - 122

print('A = ', ord("A"))
print('65 = ', chr(65))