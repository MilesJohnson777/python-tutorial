# ask user to enter a message
# convert that message into unicode / print
# display message in original format / print

mess = str.upper(input('Enter a secret message: '))

altered = ''

for i in mess:
    altered += str(ord(i))

print()
print('Your original message uppercase: ', mess)
print()
print('Your message made secret: ', altered)
print()

# this code takes that string of unicode numbers in
# sets of 2 then adds 32 to each set to make them lower case letters

lower = ''
counter = 0
temp = []
temp_string = ''

for i in range(0, len(altered)):
    if counter > 1:
        counter = 0

    temp.append(altered[i])

    counter += 1

    if counter == 2:
        for i in range(0, len(temp)):
            temp_string += temp[i]

        # this if else statement makes sure only letters are converted
        if (int(temp_string) >= 65) and (int(temp_string) <= 90):
            lower += chr(int(temp_string) + 32)
        else:
            lower += chr(int(temp_string))

        temp_string = ''
        temp = []

print('back to lower case from unicode: ', lower)
