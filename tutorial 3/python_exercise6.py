import random

while True:
    rand_num = random.randrange(1, 11)
    try:
        guess = int(input('guess the secret number between 1 and 10: '))
    except ValueError:
        print('that was not a number, try again.')
    except:
        print('An unknown error occured')
    if guess != rand_num:
        print('NOPE! try again.')
        continue
    else:
        print('You guessed it!')
        break