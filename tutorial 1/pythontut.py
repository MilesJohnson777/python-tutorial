# #comment
# '''
# multi line comment
# '''
#
# #ask for name and assign it to var called name
#
# name = input('what is your name? ')
#
# # print hello followed by the name entered
#
# print("hello ", name)
#
########################################################################
# #ask the user for 2 values and store them in variables num1 num2
# num1, num2 = input('enter 2 numbers: ').split()
# #convert the strings into regular numbers
# num1 = int(num1)
# num2 = int(num2)
# #add the values entered and store in var called sum
# sum = num1 + num2
# #subtract values and store in var called difference
# difference = num1 - num2
# #multiply values and store in var called product
# product = num1 * num2
# #divide and store on var called quotient
# quotient = num1 / num2
# #use modulus on values to find remainder
# remainder = num1 % num2
# #print the results on screen
# print('{} + {} = {}'.format(num1, num2, sum))
# print('{} - {} = {}'.format(num1, num2, difference))
# print('{} * {} = {}'.format(num1, num2, product))
# print('{} / {} = {}'.format(num1, num2, quotient))
# print('{} % {} = {}'.format(num1, num2, remainder))

########################################################################
# #ask user to enter calculation into calculator
#
# #store the user input of 2 numbers and the operator
# num1, operator, num2 = input('enter calculation: ').split()
#
# #convert the string into integers
# num1 = int(num1)
# num2 = int(num2)
#
# #if + then need to provide output based on addition and print result
# if operator == "+":
#     print("{} + {} = {}".format(num1, num2, num1+num2))
# elif operator == "-":
#     print("{} - {} = {}".format(num1, num2, num1 - num2))
# elif operator == "*":
#     print("{} * {} = {}".format(num1, num2, num1 * num2))
# elif operator == "/":
#     print("{} / {} = {}".format(num1, num2, num1 / num2))
# else:
#     print("please use an operator")

#########################################################################

#we'll provide different output based on age
#1 - 18 = important
#21, 50, >65 important
#all others are not important

#receive age and store in var
age = eval(input('enter age: '))

#if age is both greater than or equal to 1 and less than or equal to 18 -> important
if (age >= 1) and (age <= 18):
    print('important Bday!')

#if age is either or 21 or 50 -> important
elif (age == 21) or (age == 50):
    print("important Bday!")

#we check if age is less than 65 then convert true to false and vice versa
elif not(age < 65):
    print('important Bday!')

#not important is default
else:
    print("not an important Bday...")