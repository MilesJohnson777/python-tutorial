## reading / writing files, tuples

#import os

# with open('mydata.txt', mode='w', encoding='utf-8') as myFile:
#     myFile.write('some words\nlorem ipsum yada yada\nhello world, sup?')
#
# with open('mydata.txt', encoding='utf-8') as myFile:
#     #print(myFile.read()) #returns the whole file
#     #print(myFile.readline()) #returns only the first line of file
#     print(myFile.readlines()) #returns every line indexed in an array
#
# os.rename('mydata.txt', 'mydata2.txt')\
#
# os.remove('mydata2.txt')
#
# # os.mkdir('mydir')
#
# os.chdir('mydir')
#
# print('current working directory: ', os.getcwd())
#
# os.chdir('..')
#
# os.rmdir('mydir')

#######################################################################################

## reading a file line by line

# with open('mydata.txt', encoding='utf-8') as myFile:
#
#     lineNum = 1
#
#     while True:
#         line = myFile.readline()
#
#         if not line:
#             break
#
#         print('Line #', lineNum, ': ', line, end='')
#
#         lineNum += 1

#########################################################################

## tuples ~ values dont change

myTuple = (1,2,3,4,5,8,9)

print('1st value: ', myTuple[0])

print(myTuple[0:3])

print('tuple length: ', len(myTuple))

more_nums = myTuple + (432, 234, 22)

print('is 9 in tuple? ', 9 in myTuple)

for i in myTuple:
    print(i)

aList = [987, 977, 3765]
print(aList)

aTuple = tuple(aList)
print(aTuple)

aList = list(aTuple)
print(aList)

print('min: ', min(aTuple))
print('max: ', max(aTuple))

