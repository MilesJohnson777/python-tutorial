'''
create a file then read it line by line.
count total number of words per line -> print.
calculate average number of word characters per line -> print.
count total number of characters in file -> print.
'''

import os

with open('mydata.txt', mode='w', encoding='utf-8') as myFile:
    myFile.write('some words\nlorem ipsum yada yada\nhello world, sup?\n')

with open('mydata.txt', encoding='utf-8') as myFile:

    lineNum = 1
    char_num = 0
    lenAvg_list = []

    while True:
        line = myFile.readline()

        if not line:
            break

        print('Line #',lineNum,': ', line, end='')
        words = line.split()
        print('number of words in this line: ', len(words))
        lineChr_avgLen = []

        for i in words:
            char_count = 0

            for j in i:
                char_count += 1

            lineChr_avgLen.append(char_count)
            char_num += char_count

        line_chr_count = 0
        for i in lineChr_avgLen:
            line_chr_count += i

        print('average number of characters for this line: ', line_chr_count / len(lineChr_avgLen))
        print()
        lineNum += 1

    print()
    print('total number of charcters in the file: ', char_num)