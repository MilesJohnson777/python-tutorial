# create a multiplication table using list comprehensions
# 1 - 9's

multable = [[1] * 10 for i in range(10)]

for i in range(1, 10):
    for j in range(1, 10):
        multable[i][j] = i * j

for i in range(1, 10):
    for j in range(1, 10):
        print(multable[i][j], end=", ")
    print()
